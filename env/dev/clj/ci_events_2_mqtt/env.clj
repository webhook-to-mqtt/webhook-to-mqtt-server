(ns ci-events-2-mqtt.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [ci-events-2-mqtt.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[ci-events-2-mqtt started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[ci-events-2-mqtt has shut down successfully]=-"))
   :middleware wrap-dev})
