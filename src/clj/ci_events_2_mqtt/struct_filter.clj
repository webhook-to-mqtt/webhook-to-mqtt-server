(ns ci-events-2-mqtt.struct-filter)

(defn pred [[name & args]]
  (let [eq (apply partial = args)]
    (condp = name
      :eq eq
      :lt (apply partial > args)
      :gt (apply partial < args)
      :present (partial some?)
      :absent (complement some?)
      :matches (apply partial re-find args)
      :include (partial some eq)
      :not (complement (apply pred args))
      )))

(defn attr [[name p]]
  (fn [item]
    (when-let [item-value (get-in item (flatten [name]))]
      ((pred p) item-value))))

;; `clauses` has format:
;; [[:some-attribute [:pred value]]] e.g.:
;; [[:name [:matches #"foo"]] [:ref [:eq "master"]]]
(defn generate-filter [clauses]
  (fn [items]
    (reduce #(filter (attr %2) items) [] clauses)))
