(ns ci-events-2-mqtt.test.handler
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [ci-events-2-mqtt.handler :refer :all]))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 404 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response))))))
