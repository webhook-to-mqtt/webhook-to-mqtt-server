(ns ci-events-2-mqtt.test.mqtt
  (:require [clojure.test :refer :all]
            [ci-events-2-mqtt.mqtt :as mqtt]))

(deftest mqtt-config
  (testing "splits CLOUDMQTT_URL correctly"
    (let [cfg (mqtt/config "mqtt://user:pw@host.cloudmqtt.com:12345")]
      (is (= {:host-port "tcp://host.cloudmqtt.com:12345" :credentials {:username "user" :password "pw"}} cfg)))))

